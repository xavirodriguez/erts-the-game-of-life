CC = gcc
CFLAGS = -Wall -I/usr/include/ -I.
LDFLAGS = -lncurses
DEPS = gameoflife.h
OBJ = main.o init.o

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

gameoflife: $(OBJ)
	gcc  -o $@ $^  $(LDFLAGS) $(CFLAGS)

clean:
	rm -f $(OBJ) gameoflife
