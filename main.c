/*

-GAME OF LIFE-

Authors: 4 coders of the Master's Degree in Automatic Control and Robotics,
in the Polytechnical University of Catalonia cursing the curse Embedded
and Real Time Systems.

Last modification: 25th of October of 2018

Team-work strategy:

The first step was to understand the problem proposed and start to think how
to solve it. Coder 4 started to do the initial menu and the game, obtaining
the correct develop of the game. However the game is not enought optimized.
The improvements and opmitization of already done code was done by Coder 3 and 1.
One of the improvements done is the creation of an option to the user to make
a default drawing, done by Coder 2. Some other function have also been added to
add versatility to the game. The code profiling has been done by the coder 1 and 2.
Finally, after seeing that we had memory leakdes, coder 1 made some changes to
solve this matter.

We have tried to assist each other in everything that we could by specifying
what should be done and how to improve it rather than splitting the work. We
did not split the work in order not to lose time adjusting what one coder did
to the work that was previously done by another time. Moreover, we collaborate
one with each other to give ideas from coder to coder in order not to get stuck
and to advance.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>
#include <unistd.h>
#include "gameoflife.h"
#include "cell.h"


// functions -> all the explanations of the functions are before them
void init(); // initialize ncurses
void game(cell **field);
int neighbour(int y, int x);
int cellstate(int y, int x);
bool kbhit();
cell **readKyeboard(cell **field);
cell **init_field(int row, int col);
cell **initgame(int sel, cell **field);


// global variables
int row,col; // to safe the windows size
int selection;
int character;


/* int main();
It is the function in charge of execute all the program. From the initialization
of the game until it ends.
*/
int main()
{
  init(); // calls to the function init
  cell **field = init_field(row,col);
  noecho();
  cbreak();

  field = initgame(selection, field);

  game(field);

  getch();  // Wait for user input
  enditall();
}


/* void init(); - Coder 2 & 4
This function creates the main manu of the game, where 4 templates are proposed
and a possibility to create an own one to the user.
As differentiation, it can be said that it doesn't depend on the size of the
windows, the read of the size of the window is done at the beginning, and depending
on it the windows is divided in 4 quarters.
*/
void init()
{
  initall();

  getmaxyx(stdscr,row,col);	/* get the number of rows and columns */

  mvprintw(0,0,"Select between any of those templates: (write + enter)"); /* print the message at the center of the screen */
  mvprintw(2,0,"1. The R-pentomino:");
  mvaddch(row/4+1,col/4,ACS_CKBOARD);
  mvaddch(row/4-1,col/4+1,ACS_CKBOARD);
  mvaddch(row/4,col/4-1,ACS_CKBOARD);
  mvaddch(row/4,col/4,ACS_CKBOARD);
  mvaddch(row/4+1,col/4,ACS_CKBOARD);

  mvprintw(2,col/2,"2. Diehard:");
  mvaddch(row/4-1,(3*col/4)+2,ACS_CKBOARD);
  mvaddch(row/4,(3*col/4)-4,ACS_CKBOARD);
  mvaddch(row/4,(3*col/4)-3,ACS_CKBOARD);
  mvaddch(row/4+1,(3*col/4)-3,ACS_CKBOARD);
  mvaddch(row/4+1,(3*col/4)+1,ACS_CKBOARD);
  mvaddch(row/4+1,(3*col/4)+2,ACS_CKBOARD);
  mvaddch(row/4+1,(3*col/4)+3,ACS_CKBOARD);

  mvprintw(row/2-1,0,"3. Acorn:");
  mvaddch((3*row/4)-1,col/4-2,ACS_CKBOARD);
  mvaddch((3*row/4),col/4,ACS_CKBOARD);
  mvaddch((3*row/4)+1,col/4-3,ACS_CKBOARD);
  mvaddch((3*row/4)+1,col/4-2,ACS_CKBOARD);
  mvaddch((3*row/4)+1,col/4+1,ACS_CKBOARD);
  mvaddch((3*row/4)+1,col/4+2,ACS_CKBOARD);
  mvaddch((3*row/4)+1,col/4+3,ACS_CKBOARD);

  mvprintw(row/2-1,col/2,"4. Exploder:");
  mvaddch((3*row/4)-3,(3*col/4)-2,ACS_CKBOARD);
  mvaddch((3*row/4)-2,(3*col/4)-2,ACS_CKBOARD);
  mvaddch((3*row/4)-1,(3*col/4)-2,ACS_CKBOARD);
  mvaddch((3*row/4),(3*col/4)-2,ACS_CKBOARD);
  mvaddch((3*row/4)+1,(3*col/4)-2,ACS_CKBOARD);
  mvaddch((3*row/4)-3,(3*col/4),ACS_CKBOARD);
  mvaddch((3*row/4)+1,(3*col/4),ACS_CKBOARD);
  mvaddch((3*row/4)-3,(3*col/4)+2,ACS_CKBOARD);
  mvaddch((3*row/4)-2,(3*col/4)+2,ACS_CKBOARD);
  mvaddch((3*row/4)-1,(3*col/4)+2,ACS_CKBOARD);
  mvaddch((3*row/4),(3*col/4)+2,ACS_CKBOARD);
  mvaddch((3*row/4)+1,(3*col/4)+2,ACS_CKBOARD);

  mvprintw(row-3,col/3,"5. Customized:");

  refresh();		 // Print it on to the screen

  mvprintw(row-1,0,"This screen has %d rows and %d columns\n",row,col);
  mvscanw(row-3,0,"%d", &selection);
  refresh();
  mvprintw(row-2,0,"%d",selection);
  erase();
}


/* cell **init_field(int row, int col); - Coder 1 & coder 3.
This function creates a pointer based array of arrays in 2D  by allocating a memory
slot of the initial screen dimensions. Then, the initialization of the features
for each pixel are set to 0.
*/
cell **init_field(int row, int col)
{
  cell ** field = (cell **) malloc(row * sizeof (cell *));
  for (int i = 0; i < row; ++i){
    field[i] = (cell *) malloc(col * sizeof(cell));
    for (int j = 0; j < col; ++j){
      field[i][j].alive = 0;
      field[i][j].pos_x = i;
      field[i][j].pos_y = j;
      field[i][j].num_neighbors = 0;
    }
  }
  return field;
}


/*cell **initgame(int sel, cell **field); - Coder 3 & Coder 4
This function creates the main manu of the game, where 4 templates are proposed
and a possibility to create an own one to the user
*/
cell **initgame(int sel, cell **field){
  switch (sel) {
    case 2:
      mvaddch(row/2-1,col/2+2,ACS_CKBOARD);
      field[row/2-1][col/2+2].alive = 1;
      mvaddch(row/2,col/2-4,ACS_CKBOARD);
      field[row/2][col/2-4].alive = 1;
      mvaddch(row/2,col/2-3,ACS_CKBOARD);
      field[row/2][col/2-3].alive = 1;
      mvaddch(row/2+1,col/2-3,ACS_CKBOARD);
      field[row/2+1][col/2-3].alive = 1;
      mvaddch(row/2+1,col/2+1,ACS_CKBOARD);
      field[row/2+1][col/2+1].alive = 1;
      mvaddch(row/2+1,col/2+2,ACS_CKBOARD);
      field[row/2+1][col/2+2].alive = 1;
      mvaddch(row/2+1,col/2+3,ACS_CKBOARD);
      field[row/2+1][col/2+3].alive = 1;
      return field;
      // break;
    case 3:
      mvaddch(row/2-1,col/2-2,ACS_CKBOARD);
      field[row/2-1][col/2-2].alive = 1;
      mvaddch(row/2,col/2,ACS_CKBOARD);
      field[row/2][col/2].alive = 1;
      mvaddch(row/2+1,col/2-3,ACS_CKBOARD);
      field[row/2+1][col/2-3].alive = 1;
      mvaddch(row/2+1,col/2-2,ACS_CKBOARD);
      field[row/2+1][col/2-2].alive = 1;
      mvaddch(row/2+1,col/2+1,ACS_CKBOARD);
      field[row/2+1][col/2+1].alive = 1;
      mvaddch(row/2+1,col/2+2,ACS_CKBOARD);
      field[row/2+1][col/2+2].alive = 1;
      mvaddch(row/2+1,col/2+3,ACS_CKBOARD);
      field[row/2+1][col/2+3].alive = 1;
      return field;
      // break;
    case 4:
      mvaddch(row/2-2,col/2-2,ACS_CKBOARD);
      field[row/2-2][col/2-2].alive = 1;
      mvaddch(row/2-1,col/2-2,ACS_CKBOARD);
      field[row/2-1][col/2-2].alive = 1;
      mvaddch(row/2,col/2-2,ACS_CKBOARD);
      field[row/2][col/2-2].alive = 1;
      mvaddch(row/2+1,col/2-2,ACS_CKBOARD);
      field[row/2+1][col/2-2].alive = 1;
      mvaddch(row/2+2,col/2-2,ACS_CKBOARD);
      field[row/2+2][col/2-2].alive = 1;
      mvaddch(row/2-2,col/2,ACS_CKBOARD);
      field[row/2-2][col/2].alive = 1;
      mvaddch(row/2+2,col/2,ACS_CKBOARD);
      field[row/2+2][col/2].alive = 1;
      mvaddch(row/2-2,col/2+2,ACS_CKBOARD);
      field[row/2-2][col/2+2].alive = 1;
      mvaddch(row/2-1,col/2+2,ACS_CKBOARD);
      field[row/2-1][col/2+2].alive = 1;
      mvaddch(row/2,col/2+2,ACS_CKBOARD);
      field[row/2][col/2+2].alive = 1;
      mvaddch(row/2+1,col/2+2,ACS_CKBOARD);
      field[row/2+1][col/2+2].alive = 1;
      mvaddch(row/2+2,col/2+2,ACS_CKBOARD);
      field[row/2+2][col/2+2].alive = 1;
      return field;
      // break;
    case 5:
      erase();
      field = readKyeboard(field);
      // readKyeboard();
      return field;
      // break;
    default:
      mvaddch(row/2+1,col/2,ACS_CKBOARD);
      field[row/2+1][col/2].alive = 1;
      mvaddch(row/2-1,col/2+1,ACS_CKBOARD);
      field[row/2-1][col/2+1].alive = 1;
      mvaddch(row/2,col/2-1,ACS_CKBOARD);
      field[row/2][col/2-1].alive = 1;
      mvaddch(row/2,col/2,ACS_CKBOARD);
      field[row/2][col/2].alive = 1;
      mvaddch(row/2+1,col/2,ACS_CKBOARD);
      field[row/2+1][col/2].alive = 1;
      return field;
      // break;
  }
}


/* void game(cell **field); -Coder 3 & Coder 4
We can say that this function is the brain of the code. It reads the neighbours
of each cell and also the lifeness of it, and create or erase new cells acording
to the Game of Life. Until the user press Esc the code will run.
*/
void game(cell **field){

  int celly, cellx, i=0;

  while(!kbhit()){

    sleep(1);

    for (celly=0; celly<row; celly++){
      for (cellx=0; cellx<col; cellx++){
        field[celly][cellx].num_neighbors = neighbour(celly, cellx);
      }
    }

    erase();

    for (celly=0; celly<row; celly++){
      for (cellx=0; cellx<col; cellx++){
          if ((field[celly][cellx].alive == 1 && (field[celly][cellx].num_neighbors == 2 || field[celly][cellx].num_neighbors == 3)) || (field[celly][cellx].alive == 0 && field[celly][cellx].num_neighbors == 3)) {
            field[celly][cellx].alive = 1;
            mvaddch(celly,cellx,ACS_CKBOARD);
          }
          else{
            field[celly][cellx].alive = 0;
          }
      }
    }
    mvprintw(0,0,"Press Esc to quit");
    mvprintw(row-1,0,"i: %d",i+1);
    refresh();
    i++;
  }
  for (int i = 0; i < row; ++i){
          free(field[i]);
  }
  free(field);
  endwin();
}


/* int neighbour(int y, int x); -Coder 4 & Coder 1.
This function reads the neighbour of each cell introducing a coordinate it returns
a variable with the number of neighbours.
*/
int neighbour(int y, int x){
  int offsetx,offsety;
  int livecells=0;

  for (offsety=-1; offsety<=1; offsety++){
    for (offsetx=-1; offsetx<=1; offsetx++){
      if (mvinch(y+offsety, x+offsetx) == ACS_CKBOARD){
        livecells++;
      }
    }
  }
  if (mvinch(y, x) == ACS_CKBOARD){
    livecells--;
  }
  return livecells;
}

int cellstate(int y, int x){
  int livecell=0;
  if (mvinch(y, x) == ACS_CKBOARD){
    livecell=1;
  }
  return livecell;
}


/* bool kbhit(void); - Coder 3
The main objective of this function is to allow the user of the application
to end the simulation when it is wanted by pressing only the "esc" button.
*/
bool kbhit(void)
{
    int ch;
    bool r;

    // turn off getch() blocking and echo
    nodelay(stdscr, TRUE);
    noecho();

    // check for input
    ch = getch();
    // printf("%d\n", ch );
    if( ch == 27 )
    {      // input q
            r = TRUE;
            ungetch(ch);
    }
    else                // no input or another input
    {
            r = FALSE;
    }

    // restore block and echo
    echo();
    nodelay(stdscr, FALSE);
    return(r);
}


/* cell **readKyeboard(cell **field); - Coder 2
This function is used for the costumed screen. It takes the selection of the user
and uses it as initial screen.
*/
cell **readKyeboard(cell **field){
  int x,y,i=0,ch; /* Holds the coordinates */
  mvprintw(0,0,"Move the cursor with the arrows + space to draw + enter to start"); /* print the message at the center of the screen */
  move(row/2, col/2);
  getyx(stdscr,y,x);
  //  getyx(life, y, x); /* Get the coordinates from curses life window */
  while(1){
    ch=getch();
    switch(ch)
    { //move by arrows + enter
        case 10: /* enter to initialize*/
            i=1;
            break;
        case 67: /* Go right*/
            x=(x+1);
            break;
        case 68: /* Go left */
            x=(x-1);
            break;
        case 66: /* Go down */
            y=(y+1);
            break;
        case 65: /* Go up */
            y=(y-1);
            break;
        case ' ': /* Activate a cell */
            mvaddch(y,x,ACS_CKBOARD);
            field[y][x].alive = 1;
            break;
    }
    if (i==1){
      return field;
      // break;
    }
    move(y, x); /* Move the coordinates to the new location and refresh
                          the game area */
    refresh();
  }
}
